# terminal-help

People get scared of the terminal when they see the terminal & some random guy typing command there. It seems scary… So I have prepared the solution for this. Application name is **terminal help**. It is easy to install. The guide to install is down below. There will be soon a video guide also. It will tell you how you can do stuff like making, copying directory or folder. At the end of everything, it will show how you can do it in the terminal itself not opening the guide also. Feel free to report issues if there are one. Also if you have any idea to improve this project you can give me :)


## Installation

First, install git. It can be installed on the most system but if it is not (check by running git in your terminal) it can be installed in `Arch Linux` & Arch Based Distros like `Manjaro`, `EndeavourOS`, `ArcoLinux` etc by

```bash
sudo pacman -S git
```

In `Debian` based distro like `Ubuntu`, `Linux Mint`, `Pop!_OS` etc by

```bash
sudo apt-get install git
```

In `RHEL (Red Hat Enterprise Linux)` or distro like `CentOS`, `Fedora` etc by
```bash
sudo yum install git
```


Now, this step is common for any distro:
```bash
cd
git clone https://gitlab.com/AnantGupta/terminal-help.git
cd terminal-help/
./install.sh # Follow the steps of the installer & install the dependency it requires
```

## Run!

Now run it by

``` bash
cd ~/terminal-help
./term-help
```

Now follow the steps you wanna do & enjoy!
## 🚀 About Me
I am Anant Gupta. I am an 8th Grader (As of 2021). I am Desktop Linux & programming enthusiast. Also, I love reading Harry Potter.
